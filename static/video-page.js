jQuery(document).ready( function($) {
	var readmore = $('<div class="video-body-readmore"><span>Show more</span></div>').css('cursor', 'pointer');
	var comments = $('#comments');
	readmore.click(function(){
		var body = $('.awedoor-post-body')
		if ( body.hasClass('opened') ) {
			body.removeClass('opened');
			$(this)[0].innerHTML = '<span>Show more</span>';
		} else {
			body.addClass('opened');
			$(this)[0].innerHTML = '<span>Show less</span>';
		}
	})

	$('.awedoor-post-body').after(readmore)
})