jQuery(document).ready( function($) {

	var autonext = true;

	var get_video_html = function( video ) {
		return '<video id="video_'+video['post_id']+'" class="video-js vjs-default-skin" controls preload="auto" width="'+video['width']+'" height="'+video['height']+'" poster="'+video['poster']+'" data-setup="{}"><source src="'+video['webm']+'" type="video/webm"></source><source src="'+video['mp4']+'" type="video/mp4"></source></video>';
	}

	var videos_loaded = [];

	var load_video = function( count ) {
		var current_video = video_data[ count ];
		$('#video-feature')[0].innerHTML = '<h2 class="awedoor-post-title"><span class="awedoor-post-date">'+current_video['date']+' : </span><a href="'+current_video['permalink']+'">'+current_video['title']+'</a></h2>'+get_video_html( current_video )+'<div class="navigation"><span id="navigation-back">Back</span> | <span id="navigation-next">Next</span></div>';
		var id = 'video_'+current_video['post_id']
		var vf = $('#video-feature')

		vf.append( $(current_video['license_html']) )

		var body = $('<div class="awedoor-post-body video-body">'+current_video['body']+'</div>');
		
		var readmore = $('<div class="video-body-readmore"><span>Show more</span></div>').css('cursor', 'pointer');
		readmore.click(function(){
			var body = $('.video-body', $(this).parent('#video-feature'));
			if ( body.hasClass('opened') ) {
				body.removeClass('opened');
				$(this)[0].innerHTML = '<span>Show more</span>';
			} else {
				body.addClass('opened');
				$(this)[0].innerHTML = '<span>Show less</span>';
			}
		})

		vf.append( body ).append( readmore );

		VideoJS.players = {}
		var player = _V_( id )

		$('#navigation-next').click(function(){
			autonext = false;
			$('#video-feature').empty()
			var c = count + 1;
			if ( c >= video_data.length ) c = 0;
			load_video( c )
		})
		$('#navigation-back').click(function(){
			autonext = false;
			$('#video-feature')[0].innerHTML = '';
			var c = count - 1;
			if ( c < 0 ) c = video_data.length - 1;
			load_video( c )
		})
		player.addEvent( 'ended', function(){
			if ( autonext ) {
				$('#video-feature').empty()
				load_video( count + 1 )
			}
		})
		if ( count > 0 ) {
			player.ready(function(){
				this.play()
				autonext = true
			})
		}

	}

	if ( $('#video-feature').size() > 0 ) {
		load_video( 0 )
	}

})