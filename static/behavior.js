jQuery(document).ready(function($){

	var play_click = function(){
		var click_sound = document.createElement('audio');
		click_sound.setAttribute('src', awedoor['template_directory']+'/static/awedoor_click.ogg');
		click_sound.load()
		click_sound.play();
	}

	$('#widgets a').mouseover(function(){
		play_click();
	})

	$('#access a').mouseover(function(){
		play_click();
	})

	var menu = $('.menu li')
	menu.each(function() {
		var t = $(this)
		if ( t.children('ul').size() > 0 ) {
			t.addClass('menu-item-parent')
		}
	})

	var menu = $('.sub-menu')
	if ( menu.size() > 0 ) {
		menu.each(function(){
			var t = $(this)
			if ( t.is(':visible') ) {

				t.css('position', 'relative')
				var menuitems = $('> li', t)

				if ( menuitems.size() > 7 ) {
					var menulast = $(menuitems[6])
					t.css('overflow', 'hidden')
					var menulasttop = menulast.position().top + menulast.height();
					t.css('height',  menulasttop )

					if ( typeof awedoor['viewmore'] == 'undefined' ) {
						var viewmore = '...'
					} else {
						var viewmore = awedoor['viewmore']
					}

					menumore = $('<li class="menu-more"><span>'+viewmore+'</span></li>')

					menumore.click( function(){
						t.css('height', 'auto')
						$(this).hide()
					})

					menulast.before( menumore )
				}

			}

		})
	}
})