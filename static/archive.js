jQuery(document).ready(function($){

	var nomore = false;

	var main = $('#main')
	var fold = $('.awedoor-page-fold')
	if ( $('.awedoor-page-fold').size() > 0 ) {
		var height = fold.offset().top + fold.height() 

		main.css('height', height - main.offset().top )

		var more = $( '<div class="page-content-readmore" style="cursor: pointer;"><span>Show more</span></div>');

		main.css('postion', 'relative')
		more.css('position', 'absolute').css('top', height)

		more.click( function(){

			$('#pagination').remove();

			var bottomPixels = $(document).height() - $('#main').height()

			$(document).endlessScroll({

				bottomPixels: bottomPixels,
				fireOnce: true,
				fireDelay: 1800,
				loader: '',
				ceaseFire: function(i){
					if ( nomore ) {
						return true;
					}
				},
				callback: function(i) {

					$('#main').append('<div id="awedoor-loading">Loading...</div>')
					$('#awedoor-loading').hide()
					$('#awedoor-loading').slideDown(2000)

					var data = {
						action: 'posts_pagination'
					}

					if ( typeof awedoor_cat != 'undefined') {
						data['cat'] = awedoor_cat;
					} else if ( typeof awedoor_tag != 'undefined' ) {
						data['tag'] = awedoor_tag;
					} else if ( typeof awedoor_monthnum != 'undefined' ) {
						data['monthnum'] = awedoor_monthnum;
					} else if ( typeof awedoor_year != 'undefined' ) {
						data['year'] = awedoor_year;
					} else if ( typeof awedoor_day != 'undefined' ) {
						data['day'] = awedoor_day;
					} else if ( typeof awedoor_s != 'undefined' ) {
						data['s'] = awedoor_s;
					} else if ( typeof awedoor_taxonomy != 'undefined' ) {
						data['taxonomy'] = awedoor_taxonomy;
						data['taxonomy_value'] = awedoor_taxonomy_value;
					} else if ( typeof awedoor_post_type != 'undefined' ) {
						data['post_type'] = awedoor_post_type;
					} else if ( typeof awedoor_author != 'undefined' ) {
						data['author'] = awedoor_author;
					}

					data['paged'] = i+1;

					$.ajax({
						url: ajaxurl, 
						data: data, 
						success: function(response) {
							if ( response == 'false' ) {
								nomore = true;
							} else {
								$('#main').append($(response))
							}
							$('#awedoor-loading').remove()
						},
						dataType: 'html',
						error: function(){
							nomore = true;
							$('#awedoor-loading').remove()
						}

					})
				}
			});

			main.css( 'height',  'auto' )
			$(this).hide()
		})

		main.append( more )
	}

})