<?php

wp_enqueue_script('awedoor-archive', get_template_directory_uri().'/static/archive.js', array( 'jquery' ) );

wp_enqueue_script( 'jquery-endless', get_template_directory_uri().'/static/jquery.endless-scroll.js', array( 'jquery' ) );

get_header();
$count = 1;

$taxonomy_slug = get_query_var('taxonomy');
$taxonomy = get_taxonomy( $taxonomy_slug );
?>

<h2 class="awedoor-post-title"><?php
	printf( __( '%s: %s', 'awedoor' ), $taxonomy->labels->singular_name, '<span>' . single_tag_title( '', false ) . '</span>' );
?></h2>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php 
	do_action( 'awe_door_post', $post, 'summary_'.$taxonomy_slug, $count ); $count++; 
?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.', 'awedoor'); ?></p>
<?php endif; ?>

<?php awe_door_pagination(); ?>

<?php

get_footer();

?>

