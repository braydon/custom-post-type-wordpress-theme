<?php

wp_enqueue_script( "comment-reply" );

get_header();

global $post;

$count = 1;

?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); 

do_action( 'awe_door_post', $post, 'full', $count );

comments_template( '', true );

$count++;

endwhile; else: ?>

<p><?php _e('Sorry, no posts matched your criteria.', 'awedoor'); ?></p>

<?php endif; ?>

<?php

get_footer();

?>