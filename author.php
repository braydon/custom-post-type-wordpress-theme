 <?php
wp_enqueue_script('awedoor-archive', get_template_directory_uri().'/static/archive.js', array( 'jquery' ) );
wp_enqueue_script( 'jquery-endless', get_template_directory_uri().'/static/jquery.endless-scroll.js', array( 'jquery' ) );

get_header();

$author_id = get_query_var( 'author' );
$author = get_userdata( $author_id );

$count = 1;
?>

<h2 class="awedoor-post-title"><?php
	printf( __( 'Author: %s', 'awedoor' ), '<span>' . $author->display_name . '</span>' );
?></h2>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php do_action( 'awe_door_post', $post, 'summary', $count ); $count++; ?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.', 'awedoor'); ?></p>
<?php endif; ?>

<?php awe_door_pagination(); ?>

<?php

get_footer();

?>