<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />

<title><?php

	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'awedoor' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/static/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	wp_head();
?>

<?php 
	// Checks for, and assigns cookie to local variable:  
	if ( !empty($_COOKIE['style'] ) ) {
		$lightsout = $_COOKIE['style'];  
	} else {
		$lightsout = false;  
	}
	if ( $lightsout ) {
		print '<link id="style-lights-off" rel="stylesheet" href="'.get_template_directory_uri().'/style-lights-off.css" type="text/css" />';
	} 
?>

</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed">
	<header id="branding" role="banner">
			<hgroup>
				<?php 
				$header_image = get_header_image();
				if ( !empty( $header_image ) ) {
					print '<h1 id="site-title"><a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( "name", "display" ) ).'" rel="home"><img src="'.$header_image.'" alt="'.get_bloginfo('name', 'display').'"/></a></h1>';
				} else {
				?>
					<h1 id="site-title"><span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span></h1>
				<?php } ?>
				<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
			</hgroup>

			<form action="<?php print home_url( '/' ); ?>" method="get" id="main-search">
			    <fieldset>
			        <input type="text" name="s" id="s" value="<?php the_search_query(); ?>" />

			    </fieldset>
			</form>

			<nav id="access" role="navigation">

				<?php
				if ( has_nav_menu( 'primary' ) ) {
					wp_nav_menu( array( 'theme_location' => 'primary' ) );
				}
				?>



				<div class="menu-master-container">
					<ul id="menu-master" class="menu">
						<?php do_action( 'awe_door_menu' ); ?>
					</ul>
				</div> 

				<div class="menu-extra-container">
					<ul id="menu-extra" class="menu">
						<?php 
						global $wp_query;
						$post_type = isset( $wp_query->post->post_type ) ? $wp_query->post->post_type : null;
						$current = '';
						if ( $post_type == 'post' && !is_search() ) {
							$current = ' current-menu-item';
						}
						print '<li id="menu-item-blog" class="menu-item'.$current.'">';
						?>

						<?php 
							$show_on_front = get_option('show_on_front');
							if (  $show_on_front == 'page') {
								$post = get_post( get_option('page_for_posts') ); 
								$url = get_permalink( $post->ID );
							} else {
								$url = get_home_url();
							}
						?>
						<a href="<?php print $url ?>"><?php _e( 'News', 'awedoor' ); ?> </a>



							<ul class="sub-menu">
								<?php 
								$args = array(
									'orderby' => 'count',
									'order' => 'DESC',
									'title_li' => 0,
								);
								wp_list_categories( $args ); 
								?> 
							</ul>


						</li>
					</ul>
				</div> 
				<?php 

				if ( has_nav_menu( 'extra-primary' ) ) {
					wp_nav_menu( array( 'theme_location' => 'extra-primary' ) );
				}
				?>
			</nav><!-- #access -->

		   <nav id="links" role="navigation">
		    <?php 
			$args = array(
				'title_li' => __('Network', 'awedoor'),
				'title_before' => '<h2 class="network-sites-title">',
				'title_after' => '</h2>'
			);
			wp_list_bookmarks( $args ); 
			?> 
		   </nav>



	</header><!-- #branding -->

	<div id="main" class="main">

	<?php 
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	if ( ( strpos( $user_agent, 'Safari' ) && !strpos( $user_agent, 'Chrome' ) ) || strpos( $user_agent, 'MSIE' ) ) { 
	?>
	<div id="browser-message">
		<p>Your browser does not support free and open video on the Internet. <br/>Please switch to use <a href="http://www.mozilla.org/en-US/firefox/new/">Firefox</a> or <a href="https://www.google.com/chrome">Chrome</a>.</p>
	</div>
	<?php } ?>


			



