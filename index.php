<?php

wp_enqueue_script( 'awedoor-archive', get_template_directory_uri().'/static/archive.js', array( 'jquery' ) );
wp_enqueue_script( 'jquery-vgrid', get_template_directory_uri().'/static/jquery.vgrid.0.1.7.min.js', array( 'jquery' ) );

wp_enqueue_script( 'jquery-endless', get_template_directory_uri().'/static/jquery.endless-scroll.js', array( 'jquery' ) );

get_header();

$count = 1;

$post_type = get_query_var( 'post_type' );

if ( have_posts() ) : while ( have_posts() ) : the_post(); 

if ( !empty( $post_type ) ) {
	$view = 'summary_'.$post_type;
} else {
	$view = 'summary';
}

do_action( 'awe_door_post', $post, $view, $count );
$count++;

endwhile; else: 
?>
<p><?php _e('Sorry, no posts matched your criteria.', 'awedoor'); ?></p>

<?php 
endif; 

?>

<?php awe_door_pagination(); ?>

<?php 
get_footer();
?>