<?php

wp_enqueue_script('awedoor-archive', get_template_directory_uri().'/static/archive.js', array( 'jquery' ) );

wp_enqueue_script( 'jquery-endless', get_template_directory_uri().'/static/jquery.endless-scroll.js', array( 'jquery' ) );

get_header();
$count = 0;
?>

<h2 class="awedoor-post-title"><?php
	printf( __( 'Search: %s', 'awedoor' ), '<span>' . get_search_query() . '</span>' );
?></h2>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php 
$count++; 
do_action( 'awe_door_post', $post, 'summary', $count ); 

?>

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.', 'awedoor'); ?></p>
<?php get_search_form(); ?>
<?php endif; ?>

<?php awe_door_pagination(); ?>

<?php

get_footer();

?>

