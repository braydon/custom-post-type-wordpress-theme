<?php

get_header();

?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div class="awedoor-post">
<?php if ( $post->post_title != '' ) { ?>
<h2 class="awedoor-post-title">
	<?php the_title(); ?>
</h2>
<?php } ?>

<div class="awedoor-post-body">

<?php the_content(); ?>

</div>

</div>

<?php comments_template( '', true ); ?>

<?php endwhile; endif; ?>

<?php

get_footer();

?>