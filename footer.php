		</div>
		<br class="clearfix" />


		<div id="widgets">

			<?php 

			if ( is_active_sidebar( 'footer-widgets' ) ) {

				dynamic_sidebar( 'footer-widgets' ); 

			} else {
				$args = array(
					'id' => 'footer-widgets',
					'name' => 'Footer Widgets',
					'description' => 'Widget area at the footer',
					'before_widget' => '<div id="%1$s" class="awedoor-widget">',
					'after_widget' => '</div>',
					'before_title' => '<h4>',
					'after_title' => '</h4>'
				);

				the_widget('WP_Widget_Meta', array('title' => __('Meta', 'awedoor') ), $args );
	
				the_widget('WP_Widget_Calendar', array('title' => __('News Calendar', 'awedoor') ), $args );
	
				the_widget('WP_Widget_Recent_Posts', array('title' => __('Recent News', 'awedoor') , 'number' => 7), $args );

				$pages = get_wp_link_pages(); //theme-check won't pass without this
				the_widget('WP_Widget_Pages', array('title' => __('Information', 'awedoor' ) ), $args ); 
			}

			?> 

			<br style="clear:both;"/>
			
		</div>

		<div id="footer">

			<p class="awedoor-colophon">
			<?php 

			do_action( 'awe_door_license' );

			print '<br/>';

			if ( class_exists('AWE_Videos') ) {
				printf( __('Powered by %s with the %s theme and the %s plugin.', 'awedoor' ), '<a href="http://wordpress.org/">WordPress</a>', '<a href="http://wordpress.org/extend/themes/awe-door/">AWE Door</a>', '<a href="http://wordpress.org/extend/plugins/awe-video/">AWE Video</a>' ); 
			} else {
				printf( __('Powered by %s with the %s theme.', 'awedoor' ), '<a href="http://wordpress.org/">WordPress</a>', '<a href="http://wordpress.org/extend/themes/awe-door/">AWE Door</a>'); 
			}

			?>

			</p>


		</div>
</div>



<?php wp_footer(); ?>
</body>
</html>
