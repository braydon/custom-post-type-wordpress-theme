<?php

// Add Templates for Video
if ( class_exists( 'AWE_Videos' ) ) {
	add_action( 'awe_door_menu', 'awe_door_video_menu' );
	add_action( 'awe_door_post', 'awe_door_video_post', 0, 3 );
	add_action( 'awe_door_license', 'awe_door_video_license' );
	add_action( 'awe_door_image_sizes', 'awe_door_video_image_sizes' );
	add_action( 'awe_door_scripts', 'awe_door_video_scripts');
	add_shortcode( 'awe_door_television', 'awe_door_video_television' );
}

function awe_door_video_scripts(){
	wp_enqueue_script( 'videojs', get_template_directory_uri().'/static/video-js/video.js' );
	wp_enqueue_style( 'videojs-css', get_template_directory_uri().'/static/video-js/video-js.css' );
	wp_enqueue_script( 'awe-door-television', get_template_directory_uri().'/static/awe-door-television.js', array( 'jquery' ) );
	wp_enqueue_style( 'video', get_template_directory_uri().'/static/awe-door-video.css' );	
}

function awe_door_video_image_sizes() {
	add_image_size( 'awevideo-poster', 697, 392, true );
	add_image_size( 'awevideo-thumbnail', 218, 123, true );
}

add_action( 'awe_door_image_sizes', 'awe_door_image_sizes' );
function awe_door_image_sizes() {
	add_image_size( 'awedoor-thumbnail', 138, 1000, false );
	add_image_size( 'awedoor-body', 697, 2000, false );
}

function awe_door_video_license() {
	$license = get_option('video_license');
	print 'Videos are available under the <a href="'.$license['url'].'">'.$license['title'].'</a> license; additional terms may apply.';
}

function awe_video_url(){
	if ( get_option('permalink_structure') != '' ) {
		return get_home_url().'/video/';
	} else {
		return get_home_url().'?post_type=video';
	}
}

function awe_door_video_menu() {

	global $wp_query;
	$post_type = isset( $wp_query->post->post_type ) ? $wp_query->post->post_type : null;
	$current = '';
	if ( $post_type == 'video' && !is_search() ) {
		$current = ' current-menu-item';
	} 
	print '<li id="menu-item-archive" class="menu-item'.$current.'">';
?>

	<a href="<?php print awe_video_url(); ?>"><?php _e( 'Video', 'awevideo' ); ?></a>
	<?php 


		$topics = get_terms('topic', array('orderby' => 'count', 'order' => 'DESC' ) );

		if ( count( $topics ) > 0 ) {
			print '<ul id="menu-item-tags" class="sub-menu">';
			$count = 0;
			$max = 7;
			foreach ( $topics as $topic ) {
				if ( $count < $max ) {
					print '<li class="menu-item">';
					print '<a href="'.get_term_link($topic->slug, 'topic').'">'.$topic->name.'</a>';
					print '</li>';
				}
				$count++;
			}
		}
		if ( $count > $max ) {
			print '<li class="menu-item menu-viewall"><a href="">View All</a></li>';
		}
		print '</ul>';

	?>

</li>

<?php } ?>

<?php 

function awe_door_video_post( $post, $view, $count ) {

	if ( $post->post_type == 'video' ) {

		$image_id = get_post_thumbnail_id( $post->ID );
		$poster = wp_get_attachment_image_src( $image_id, 'awevideo-poster' );
		$video = AWE_Videos::retrieve($post->ID);


	if ( $view == 'summary_topic' || $view == 'summary_video' ) {

			if ( is_sticky( $post->ID ) ) {
				$sticky_class = ' sticky';
			} else {
				$sticky_class = '';
			}
			if ( $count == 9 ) {
				$fold = ' awedoor-page-fold';
			} else {
				$fold = '';
			}

			print '<div class="video-thumbnail post-'.$post->ID.$sticky_class.' post-count-'.$count.$fold.'">';
		?>

		<?php
			$image_id = get_post_thumbnail_id( $post->ID );
	
			$poster = wp_get_attachment_image_src( $image_id, 'awevideo-thumbnail' );
	
			print '<a href="'.get_permalink().'"><img src="'.$poster[0].'" width="'.$poster[1].'" height="'.$poster[2].'"/></a>';

		?>

			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

		</div>
		<?php
	} else if ( $view == 'summary' ) {

			if ( is_sticky( $post->ID ) ) {
				$sticky_class = ' sticky';
			} else {
				$sticky_class = '';
			}
			if ( $count == 3 ) {
				$fold = ' awedoor-page-fold';
			} else {
				$fold = '';
			}
			print '<div class="awedoor-post-summary post-'.$post->ID.$sticky_class.' post-count-'.$count.$fold.'">';
			$uri = get_permalink( $post->ID );
			print '<h2 class="awedoor-post-summary-title"><span class="awedoor-post-summary-date">'.mysql2date(get_option('date_format'), $post->post_date).' : </span><a href="'.$uri.'">'.apply_filters( 'the_title', $post->post_title ).'</a></h2>';

			if ( has_post_thumbnail( $post->ID ) ) {
				$image_id = get_post_thumbnail_id( $post->ID );
				$image = wp_get_attachment_image_src( $image_id, 'awedoor-thumbnail' );

				print '<div class="awedoor-post-summary-thumbnail-video"><a href="'.$uri.'"><img style="background-image:url('.$image[0].');" src="'.get_template_directory_uri().'/static/video_overlay.png'.'" width="138" height="78" alt="[]" /></a></div>';

			}

			print get_post_excerpt( $post, 40 );

			print '<div class="more-link-wrapper"><a href="'.$uri.'">'.__( 'View More', 'awedoor' ).'</a></div>';


			print '<div class="awedoor-post-summary-meta">';
			print '<div class="awedoor-post-categories-wrapper">Topics: <ul class="awedoor-post-categories">';
			$topics = wp_get_post_terms( $post->ID, 'topic' );
			$total = count( $topics );
			$topics = array_splice( $topics, 0, 7 );
			if ( count( $topics ) > 0 ) {
				foreach( $topics as $topic ) {
					print '<li class="awedoor-post-summary-taxonomy"><a href="'.get_term_link( $topic->slug, 'topic' ).'">'.$topic->name.'</a></li>';
				};
				if ( $total > count( $topics ) ) print '<li>...</li>';
				print '</ul></div>';
				
			}


			print '</div>';
			print '</div>';


	} else {

		wp_enqueue_script( 'video-page', get_template_directory_uri().'/static/video-page.js', array( 'jquery' ) );


		?>
			<article class="awedoor-post">
			<h2 class="awedoor-post-title"><span class="awedoor-post-date"><?php the_date(); ?> : </span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

			<video id="video_<?php print $post->ID; ?>" class="video-js vjs-default-skin" controls
			  preload="auto" autoplay="true" width="697" height="<?php print round($video['aspect'] * 697); ?>" poster="<?php print $poster[0]; ?>"
			  data-setup="{}">
			  <source src="<?php print $video['webm']; ?>" type='video/webm'>
			  <source src="<?php print $video['mp4']; ?>" type='video/mp4'>
			</video>

			<?php print AWE_Videos::license_html( $post ); ?>

			<div class="awedoor-post-body video-body">
				<?php the_content(); ?>
			</div>
			<?php

			print '<div class="awedoor-post-summary-meta">';
			print '<div class="awedoor-post-categories-wrapper">';


			$cats = get_the_terms( $post->ID, 'topic' );
			$total = count( $cats );
			$cats = array_splice( $cats, 0, 7 );
			if ( count( $cats ) > 0 ) {
				print 'Topics: <ul class="awedoor-post-categories">';
				foreach( $cats as $cat ) {
					print '<li class="awedoor-post-summary-taxonomy"><a href="'.get_term_link( $cat->slug, 'topic' ).'">'.$cat->name.'</a></li>';
				};
				if ( $total > count( $cats ) ) print '<li>...</li>';
				print '</ul></div>';
				
			}

			if (! comments_open() && 
				! is_page() && 
				post_type_supports( get_post_type(), 'comments' ) ) {
				print '<div class="nocomments">'.__( 'Comments are closed.', 'awedoor' ).'</div>';
			} else {
				print '<a class="awedoor-post-summary-comments" href="'.get_permalink($post->ID).'#comments">';
				comments_number( '', '1 Comment', '% Comments' );
				print '</a>';
			}

			print '</div>';

			?>



			</article>

		<?php 
		}
	}

}

function awe_door_video_television() {

	global $posts;
	query_posts('post_type=video&posts_per_page=-1');
	$video_data = array();
	global $post;

?>

	<div id="video-feature"></div>

	<script>

	<?php 

	if ( have_posts() ) : while ( have_posts() ) : the_post(); 

		$image_id = get_post_thumbnail_id( $post->ID );
		$poster = wp_get_attachment_image_src( $image_id, 'video-poster' );
		$video = AWE_Videos::retrieve($post->ID);

		$width = 697;
		$height = round( $video['aspect'] * $width );

		$video_data[] = array(
			'title' => get_the_title(), 
			'license_html' => AWE_Videos::license_html( $post ),
			'date' => get_the_date(), 
			'body' => apply_filters( 'the_content', get_the_content() ), 
			'post_id' => $post->ID, 
			'permalink' => get_permalink(),
			'webm' => $video['webm'],
			'mp4' => $video['mp4'],
			'poster' => $poster[0],
			'width' => $width,
			'height' => $height
		);

	endwhile; endif; 

	print 'var video_data = ' . json_encode( $video_data );

	?>

	</script>

<?php 
	wp_reset_query();
} 
?>


<?php

add_action( 'wp_enqueue_scripts', 'awe_door_enqueue_scripts' );

function awe_door_enqueue_scripts(){
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'behavior',  get_template_directory_uri().'/static/behavior.js', array('jquery'));
	wp_localize_script( 'behavior', 'awedoor', array('viewmore' => __('Show more', 'awedoor' ), 'template_directory' => get_template_directory_uri() ) );
	wp_enqueue_script( 'jquery-easing', get_template_directory_uri().'/static/jquery.easing.1.3.js', array( 'jquery' ) );
	wp_enqueue_script( 'jquery-vgrid', get_template_directory_uri().'/static/jquery.vgrid.0.1.7.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'footer', get_template_directory_uri().'/static/footer.js', array( 'jquery-vgrid' ) );

	do_action( 'awe_door_scripts' );
}

register_sidebar(array(
    'id' => 'footer-widgets',
    'name' => 'Footer Widgets',
    'description' => 'Widget area at the footer',
    'before_widget' => '<div id="%1$s" class="awedoor-widget">',
    'after_widget' => '</div>',
    'before_title' => '<h4>',
    'after_title' => '</h4>'
));

if ( ! isset( $content_width ) ) $content_width = 697;

add_action( 'after_setup_theme', 'my_theme_setup' );
function my_theme_setup(){
	load_theme_textdomain( 'awedoor', get_template_directory() . '/languages' );
}

define( 'BACKGROUND_COLOR', 'ffe' );

add_custom_background();

define( 'NO_HEADER_TEXT', true );
define( 'HEADER_TEXTCOLOR', '000' );
//define( 'HEADER_IMAGE', '' );
define( 'HEADER_IMAGE_WIDTH', apply_filters( 'awedoor_header_image_width', 220 ) );
define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'awedoor_header_image_height', 220 ) );

add_theme_support('custom-header');
add_custom_image_header( 'awedoor_header_style', 'awedoor_admin_header_style', 'awedoor_admin_header_image' );



function awedoor_admin_header_image() { ?>
	<div>
		<?php 
		$header_image = get_header_image();
		if ( !empty( $header_image ) ) {
			print '<h1 id="site-title"><a href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( "name", "display" ) ).'" rel="home"><img src="'.$header_image.'" alt="'.get_bloginfo('name', 'display').'"/></a></h1>';
		} else {
		?>
			<h1 id="site-title"><span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span></h1>
		<?php } ?>
		<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
	</div>
<?php }

function awedoor_header_style() {
}

function awedoor_admin_header_style() {
	?>
	<style>

		@font-face { 
			font-family: 'Allerta';
			src: local('Allerta Regular'),
				local('Allerta-Regular'),
				url('<?php print get_template_directory_uri()."/static/fonts/allerta/allerta-regular.woff"; ?>') format('woff');
			font-weight: normal;
			font-style: normal;
		}

		#site-title a {
			color:#000;
			text-decoration:none;
		}

		#site-title {
			font-family:FreeSans, Arial, sans-serif;
			font-size:26px;
			letter-spacing:-2px;
			line-height:32px;
			font-weight:normal;
			margin-bottom:10px;
			margin-top:16px;
			overflow:hidden;
			width:220px;
		}
		#site-description {
			color:#A00;
			font-family:FreeSans, Arial, sans-serif;
			font-size:13px;
			font-weight:normal;
			margin-top:5px; 
			text-transform:uppercase;
		}
	</style>
	<?php
}

add_theme_support('automatic-feed-links');
add_theme_support('post-thumbnails');
//add_theme_support('post-formats', array( 'gallery' ) );

do_action( 'awe_door_image_sizes' );

register_nav_menus(
	array( 'top' => __( 'Top Menu', 'awedoor' ), 'bottom' => __( 'Bottom Menu', 'awedoor' ) )
);

add_action( 'awe_door_post', 'awe_door_post', 0, 3 );

if ( is_admin() ) {
	add_action( 'wp_ajax_posts_pagination', 'awe_door_ajax_posts_pagination', 0 );
	add_action( 'wp_ajax_nopriv_posts_pagination', 'awe_door_ajax_posts_pagination', 0 );
}

add_action('wp_head','awe_door_ajaxurl');

function awe_door_ajaxurl() {
	global $wp_query;

	print '<script type="text/javascript">';
	print 'var ajaxurl = "'.admin_url('admin-ajax.php').'";';
	if ( is_tag() ) {
		print 'var awedoor_tag = "'.get_query_var('tag').'";';
	} else if ( is_year() ) {
		print 'var awedoor_year = "'.get_query_var('year').'";';
	} else if ( is_month() ) {
		print 'var awedoor_year = "'.get_query_var('year').'";';
		print 'var awedoor_monthnum = "'.get_query_var('monthnum').'";';
	} else if ( is_day() ) {
		print 'var awedoor_year = "'.get_query_var('year').'";';
		print 'var awedoor_monthnum = "'.get_query_var('monthnum').'";';
		print 'var awedoor_day = "'.get_query_var('day').'";';
	} else if ( is_category() ) {
		print 'var awedoor_cat = "'.get_query_var('cat').'";';
	} else if ( is_search() ) {
		print 'var awedoor_s = "'.get_query_var('s').'";';
	} else if ( is_author() ) {
		print 'var awedoor_author = "'.get_query_var('author').'";';
	} else if ( isset( $wp_query->query_vars['taxonomy'] )  ) {
		$tax = get_query_var('taxonomy');
		print 'var awedoor_taxonomy = "'.$tax.'";';
		print 'var awedoor_taxonomy_value = "'.get_query_var($tax).'";';
	} else if ( isset( $wp_query->query_vars['post_type'] )  ) {
		$post_type = get_query_var('post_type');
		print 'var awedoor_post_type = "'.$post_type.'";';
	}
	print '</script>';
}

function awe_door_ajax_posts_pagination(){

	$args = array(
		'cat' => $_REQUEST['cat'] ? $_REQUEST['cat'] : '',
		'tag' => $_REQUEST['tag'] ? $_REQUEST['tag'] : '',
		'monthnum' => $_REQUEST['monthnum'] ? $_REQUEST['monthnum'] : '',
		'year' => $_REQUEST['year'] ? $_REQUEST['year'] : '',
		'day' => $_REQUEST['day'] ? $_REQUEST['day'] : '',
		's' => $_REQUEST['s'] ? $_REQUEST['s'] : '',
		'author' => $_REQUEST['author'] ? $_REQUEST['author'] : '',
		'post_type' => $_REQUEST['post_type'] ? $_REQUEST['post_type'] : '',
		'paged' => $_REQUEST['paged']
	);

	if ( isset( $_REQUEST['taxonomy'] ) && isset( $_REQUEST['taxonomy_value'] ) ) {
		$args[ $_REQUEST['taxonomy'] ] = $_REQUEST['taxonomy_value'];
	}

	query_posts( $args );

	if ( have_posts() ) : while ( have_posts() ) : the_post(); 

		global $post;

		if ( !empty( $args['post_type'] ) ) {
			$view = 'summary_'.$args['post_type'];
		} else if ( isset( $args['taxonomy'] ) ) {
			$view = 'summary_'.$taxonomy;
		} else {
			$view = 'summary';
		}

		do_action( 'awe_door_post', $post, $view, 0 );
	
	endwhile; else:

	header( "HTTP/1.0 404 Not Found" );

	endif;

	exit();	
}

function awe_door_pagination(){

?>
	<nav id="pagination">
		<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'awedoor' ) ); ?></div>
		<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'awedoor' ) ); ?></div>
	</nav>

<?php

}

function awe_door_post( $post, $view, $count ) {
	if ( $post->post_type == 'post' ) {
		if ( $view == 'summary' ) {
			if ( is_sticky( $post->ID ) ) {
				$sticky_class = ' sticky';
			} else {
				$sticky_class = '';
			}
			if ( $count == 3 ) {
				$fold = ' awedoor-page-fold';
			} else {
				$fold = '';
			}
			print '<div class="awedoor-post-summary post-'.$post->ID.$sticky_class.' post-count-'.$count.$fold.' '.get_post_class().'">';

			$uri = get_permalink( $post->ID );
			print '<h2 class="awedoor-post-summary-title"><span class="awedoor-post-summary-date">'.mysql2date(get_option('date_format'), $post->post_date).' : </span><a href="'.$uri.'">'.apply_filters( 'the_title', $post->post_title ).'</a></h2>';

			if ( has_post_thumbnail( $post->ID ) ) {
				$image_id = get_post_thumbnail_id( $post->ID );
				$image = wp_get_attachment_image_src( $image_id, 'awedoor-thumbnail' );

				print '<a href="'.$uri.'"><img class="awedoor-post-summary-thumbnail" src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" alt="[]" /></a>';

			}
			
	
			print get_post_excerpt( $post, 40 );

			if ( !has_post_thumbnail( $post->ID ) ){

				$image_posts = &get_children( array( 
					'post_parent' => $post->ID, 
					'post_type' => 'attachment', 
					'post_mime_type' => 'image' 
					) 
				);

				if ( !empty( $image_posts ) ) {
					foreach ( $image_posts as $attachment_id => $attachment ) {
						$image = wp_get_attachment_image_src( $attachment_id, 'awedoor-thumbnail' );
						$url = get_permalink( $attachment_id );
						print '<a href="'.$url.'"><img class="awedoor-post-summary-gallery-thumbnail" src="'.$image[0].'" width="'.$image[1].'" height="'.$image[2].'" ></a>';
					}

				}
			}


			print '<div class="more-link-wrapper"><a href="'.$uri.'">'.__( 'View more', 'awedoor' ).'</a></div>';


			print '<div class="awedoor-post-summary-meta">';
			print '<div class="awedoor-post-categories-wrapper">';
			$cats = get_the_category( $post->ID );
			$total = count( $cats );
			$cats = array_splice( $cats, 0, 7 );
			if ( count( $cats ) > 0 ) {
				print 'Categories: <ul class="awedoor-post-categories">';
				foreach( $cats as $cat ) {
					print '<li class="awedoor-post-summary-taxonomy"><a href="'.get_term_link( $cat->slug, 'category' ).'">'.$cat->name.'</a></li>';
				};
				if ( $total > count( $cats ) ) print '<li>...</li>';
				print '</ul></div>';
				
			}

			$tags = wp_get_post_tags( $post->ID );
			$total = count( $tags );
			$tags = array_splice( $tags, 0, 7 );
			if ( count( $tags ) > 0 ) {
				print '<div class="awedoor-post-categories-wrapper">Tags: <ul class="awedoor-post-categories">';
				foreach( $tags as $tag ) {
					print '<li class="awedoor-post-summary-taxonomy"><a href="'.get_term_link( $tag->slug, 'post_tag' ).'">'.$tag->name.'</a></li>';
				};
				if ( $total > count( $tags ) ) print '<li>...</li>';
				print '</ul></div>';
			}

			if (! comments_open() && 
				! is_page() && 
				post_type_supports( get_post_type(), 'comments' ) ) {
				print '<div class="nocomments">'.__( 'Comments are closed.', 'awedoor' ).'</div>';
			} else {
				print '<a class="awedoor-post-summary-comments" href="'.get_permalink($post->ID).'#comments">';
				comments_number( '', '1 Comment', '% Comments' );
				print '</a>';
			}

			print '</div>';

			print '</div>';
		} else if ( $view == 'full' ) {

			$uri = get_permalink( $post->ID );
			$date = get_the_date();
			print '<article class="awedoor-post">';
			print '<h2 class="awedoor-post-title"><span class="awedoor-post-date">'.$date.' : </span>'.apply_filters( 'the_title', $post->post_title ).'</h2>';


			$author = get_userdata( $post->post_author );
			print '<div class="awedoor-post-author">By <a href="'.get_author_posts_url($author->ID).'">'.$author->display_name.'</a></div>';
			print '<div class="awedoor-post-body">';
			the_content();

			//.apply_filters( 'the_content', $post->post_content ).'';

			print '<br class="clearfix"/>';

			wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) );

			print '<br class="clearfix"/>';



			print '</div>';



			print '<div class="awedoor-post-meta">';
			print '<div class="awedoor-post-categories-wrapper">Categories: <ul class="awedoor-post-categories">';
			$cats = get_the_category( $post->ID );
			if ( count( $cats ) > 0 ) {
				foreach( $cats as $cat ) {
					print '<li class="awedoor-post-taxonomy"><a href="'.get_term_link( $cat->slug, 'category' ).'">'.$cat->name.'</a></li>';
				};
				print '</ul></div>';
			}


			$tags_list = get_the_tag_list(); //theme-check won't pass without this
			$tags = wp_get_post_tags( $post->ID );
			if ( count( $tags ) > 0 ) {
				print '<div class="awedoor-post-categories-wrapper">Tags: <ul class="awedoor-post-categories">';
				foreach( $tags as $tag ) {
					print '<li class="awedoor-post-taxonomy"><a href="'.get_term_link( $tag->slug, 'post_tag' ).'">'.$tag->name.'</a></li>';
				};
				print '</ul></div>';
			}

			if (! comments_open() && 
				! is_page() && 
				post_type_supports( get_post_type(), 'comments' ) ) {
				print '<div class="nocomments">'.__( 'Comments are closed.', 'awedoor' ).'</div>';
			} else {
			?>

				<?php
					printf( _n( 'One comment', '%1$s comments', get_comments_number(), 'awedoor' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
				?>

			<?php
			}

			print '</div>';

			print '<br class="clearfix"/>';

			print '<div class="awedoor-post-body-nav">';


			$prev = get_next_post();

			if ( $prev != null ) {
				print '<div class="awedoor-post-body-nav-prev">';			  
				print '<h4>Previous</h4>';
				$url = get_permalink( $prev->ID );
				print '<a href="'.$url.'">'.apply_filters( 'the_title', $prev->post_title ).'</a>';
				print '<div class="awedoor-post-prev">';
				print get_post_excerpt( $prev, 40 );
				print '<div class="more-link-wrapper"><a href="'.$url.'">'.__('View more', 'awedoor').'</a></div>';
				print '</div>';
				print '</div>';
			}



			$next = get_previous_post();

			if ( $next != null ) {
				print '<div class="awedoor-post-body-nav-next">';
				print '<h4>Next</h4>';
				$url = get_permalink( $next->ID );
				print '<a href="'.$url.'">'.apply_filters('the_title', $next->post_title ).'</a>';
				print '<div class="awedoor-post-prev">';
				print get_post_excerpt( $next, 40 );
				print '<div class="more-link-wrapper"><a href="'.$url.'">'.__('View more', 'awedoor').'</a></div>';
				print '</div>';
				print '</div>';
			}



			print '<br class="clearfix"/>';


			print '</div>';




			print '</article>';

		}
	} else if ( $post->post_type == 'page' ) {
		if ( $view == 'summary' ) {
			if ( is_sticky( $post->ID ) ) {
				$sticky_class = ' sticky';
			} else {
				$sticky_class = '';
			}
			print '<div class="awedoor-post-summary post-'.$post->ID.$sticky_class.' post-count-'.$count.'">';

			$uri = get_permalink( $post->ID );
			print '<h2 class="awedoor-post-summary-title"><span class="awedoor-post-summary-date">'.mysql2date(get_option('date_format'), $post->post_date).' : </span><a href="'.$uri.'">'.apply_filters( 'the_title', $post->post_title ).'</a></h2>';

			print get_post_excerpt( $post, 40 );
	
			print '</div>';
		}		
	} else if ( $post->post_type == 'attachment' ) {
		print '<article class="awedoor-post">';
		if ( strpos( $post->post_mime_type, 'image' ) >= 0 ) {

			print '<h2 class="awedoor-post-title"><span>'.get_the_date().' </span>';
			if ( $post->post_title ) {
				print ' : '.$post->post_title;
			}
			print '</h2>'; // title
			$author = get_userdata( $post->post_author ); // author id
			print '<div class="awedoor-post-author"><a href="'.get_author_posts_url( $post->post_author ).'">'.$author->display_name.'</a></div>';
			$image_src = wp_get_attachment_image_src( $post->ID, 'awedoor-body' );
			$img_alt = get_post_meta( $post->ID, '_wp_attachment_image_alt', true); //alt
			if ($img_alt == '') {
				$img_alt = $post->post_title;
			}
			print '<div class="awedoor-post-attachment"><img src="'.$image_src[0].'" width="'.$image_src[1].'" height="'.$image_src[2].'" alt="['.$img_alt.']"></div>';
			print '<p class="wp-caption-text">'.$post->post_excerpt.'</p>'; // caption
			
			print '<div class="awedoor-post-body awedoor-post-bodyfull">';

			print '<p>'.$post->post_content.'</p>'; // description

			print '<div class="awedoor-post-attachment-imagesizes">';
			print '<h3>Image Sizes</h3>';

			//$image_sizes = get_intermediate_image_sizes();
			$image_sizes = array('thumbnail','medium','large', 'original');
			print '<table>';
			print '<tr><th>'.__('Width').'</th><th>'.__('Height').'</th><th>'.__('Title').'</th></th></tr>';
			foreach ( $image_sizes as $image_size ) {
				$image_src = wp_get_attachment_image_src( $post->ID, $image_size );

				print '<tr><td>'.$image_src[1].'</td><td>'.$image_src[2].'</td><td>'.$image_size.'</td><td><a href="'.$image_src[0].'">'.__('View', 'awedoor').'</a></td></tr>';
			}
			print '</table>';
			print '</div>';



			print '<div class="awedoor-post-attachment-nav">';
			print '<h3>Navigation</h3>';

			$attachments = array_values( get_children( array(
				'post_parent' => $post->post_parent, 
				'post_status' => 'inherit', 
				'post_type' => 'attachment', 
				'post_mime_type' => 'image', 
				'order' => 'ASC', 
				'orderby' => 'menu_order ID'
			)));

			foreach ( $attachments as $k => $attachment ) {
				if ( $attachment->ID == $post->ID ) {
					break;
				}
			}

			$prev = $k - 1;
			$next = $k + 1;

			if ( isset($attachments[$prev]) ) {
				$img = wp_get_attachment_image_src( $attachments[$prev]->ID, 'awedoor-thumbnail' );
				$url = get_attachment_link( $attachments[$prev]->ID );
				print '<div class="awedoor-post-attachment-prev"><a href="'.$url.'"><img src="'.$img[0].'" width="'.$img[1].'" height="'.$img[2].'" alt="[]"></a><span class="awedoor-post-nav"><a href="'.$url.'">Previous</a></span></div>';

			}

			if ( isset($attachments[$next]) ) {
				$img = wp_get_attachment_image_src( $attachments[$next]->ID, 'awedoor-thumbnail' );
				$url = get_attachment_link( $attachments[$next]->ID );
				print '<div class="awedoor-post-attachment-next"><a href="'.$url.'"><img src="'.$img[0].'" width="'.$img[1].'" height="'.$img[2].'" alt="[]"></a><span class="awedoor-post-nav"><a href="'.$url.'">Next</a></span></div>';
			}

			print '</div>';

			print '</div>';



			print '<br class="clearfix"/>';




		} else {
			print '<a href="'.wp_get_attachment_url( $post->ID ).'">'.wp_get_attachment_url( $post->ID ).'</a>';
		}
		print '</article>';
	}
}

function get_post_excerpt($item, $excerpt_length=55, $excerpt_more='...') {
	$text = $item->post_excerpt;
	$raw_excerpt = $text;

	if ( '' == $text ) {
		$text = $item->post_content;
        
		$text = strip_shortcodes($text);
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = strip_tags($text);
		$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);

		if ( count($words) > $excerpt_length ) {
			array_pop($words);
			$text = implode(' ', $words);
			$text = $text . $excerpt_more;
		} else {
			$text = implode(' ', $words);
		}
	}
	$text = apply_filters('get_the_excerpt', $text);
	$text = apply_filters('the_excerpt', $text);
	return $text;
}


if ( ! function_exists( 'awedoor_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own awedoor_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since AWE Door 0.1
 */
function awedoor_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p>&#8734; <?php _e( 'Pingback:', 'awedoor' ); ?> <?php comment_author_link(); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php
						$avatar_size = 32;
						if ( '0' != $comment->comment_parent )
							$avatar_size = 32;

						echo get_avatar( $comment, $avatar_size );

						/* translators: 1: comment author, 2: date and time */
						printf( __( '%1$s / %2$s says:', 'awedoor' ),
							sprintf( '<a class="comment-date" href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),
								/* translators: 1: date, 2: time */
								sprintf( __( '%1$s at %2$s', 'awedoor' ), get_comment_date(), get_comment_time() )
							),
							sprintf( '<span class="fn">%s</span>', get_comment_author_link() )

						);
					?>


				</div><!-- .comment-author .vcard -->

				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'awedoor' ); ?></em>
					<br />
				<?php endif; ?>

			</footer>

			<div class="comment-body"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'awedoor' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			<?php edit_comment_link( __( 'Edit', 'awedoor' ), '', '' ); ?>
			</div><!-- .reply -->

		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for awedoor_comment()

?>